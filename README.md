# GUX

## Comenzando 🚀
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

-   Docker 19.03^
-   Docker Compose 1.25^
***

## Instalación 🔧

1. Clonar el repositorio
```
git clone git@gitlab.com:linets/ecommerce/middlewares/gux.git
```
ó
```
git clone https://gitlab.com/linets/ecommerce/middlewares/gux.git
```

2. Posicionarse en la rama _develop_
```
git checkout develop
```
3. Crear copia de variables de entorno
 ```
cp -r .envs.example .envs
```
4. Build
```
docker-compose build
```
5. Levantar el contenedor
```docker-compose up``` ó ```docker-compose up -d``` para ejecutar el contenedor en segundo plano
6. Verificar la instalacion
```
http://localhost:8000/
```
7. Cree su base de datos de replica, agrege sus credencialse en el .web.env
```
CREATE DATABASE nombre_de_la_base_de_datos;
```
8. Correr las migraciones en ambas base de datos
```
Ejecuata estos comandos en el contenedor web:

python manage.py migrate
python manage.py migrate --database=db_replica
```
9. Revisar la documentación
```
http://127.0.0.1:8000/api/schema/redoc
```
> En caso de no poder ver el sitio detener el contenedor y levantarlo nuevamente
***
