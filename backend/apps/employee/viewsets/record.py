# Third Parties
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

# Local
from employee.models import EmploymentRecord
from employee.serializers import EmploymentRecordSerializer


class EmploymentRecordViewSet(viewsets.ModelViewSet):
    """
    Viewset model crud of employment records.
    """
    queryset = EmploymentRecord.objects.all().order_by('id')
    serializer_class = EmploymentRecordSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'employee': ['exact'],
    }

