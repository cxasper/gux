# Third Parties
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

# Local
from employee.models import Employee
from employee.paginations import CustomPagination
from employee.serializers import EmployeeSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    Viewset model crud of employee.
    """
    queryset = Employee.objects.all().order_by('name')
    serializer_class = EmployeeSerializer
    pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = [
        'name', 'dni', 'birthday', 'nationality'
    ]
    filterset_fields = {
        'birthday': ['exact'],
        'dni': ['exact']
    }
