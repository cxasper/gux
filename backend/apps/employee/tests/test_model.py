# -*- coding: utf-8 -*-
from django.test import TestCase

from employee.models import Employee, EmploymentRecord
from employee.tests.factories import EmployeeFactory, EmploymentRecordFactory


class EmployeeTestCase(TestCase):

    def setUp(self):
        self.employee = EmployeeFactory()
        self.field_list = [
            field.name for field in Employee._meta.get_fields()
        ]

    def test_have_fields_needed_by_the_business(self):
        self.assertTrue('id' in self.field_list)
        self.assertTrue('name' in self.field_list)
        self.assertTrue('dni' in self.field_list)
        self.assertTrue('birthday' in self.field_list)
        self.assertTrue('position' in self.field_list)
        self.assertTrue('profession' in self.field_list)
        self.assertTrue('has_children' in self.field_list)
        self.assertTrue('nationality' in self.field_list)

    def test_method_str(self):
        self.assertEqual(f'{self.employee.name} - {self.employee.dni}', str(self.employee))

    def tearDown(self):
        self.employee.delete()


class EmploymentRecordTestCase(TestCase):

    def setUp(self):
        self.employment_record = EmploymentRecordFactory()
        self.field_list = [
            field.name for field in EmploymentRecord._meta.get_fields()
        ]

    def test_have_fields_needed_by_the_business(self):
        self.assertTrue('id' in self.field_list)
        self.assertTrue('employee' in self.field_list)
        self.assertTrue('company_name' in self.field_list)
        self.assertTrue('duration' in self.field_list)
        self.assertTrue('position' in self.field_list)

    def test_method_str(self):
        self.assertEqual(f'{self.employment_record.company_name}', str(self.employment_record))

    def tearDown(self):
        self.employment_record.delete()
