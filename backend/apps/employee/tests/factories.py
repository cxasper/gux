# -*- coding: utf-8 -*-
import factory
from faker import Faker
from employee.models import Employee, EmploymentRecord

faker = Faker('es_ES')


class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Employee

    name = factory.LazyAttribute(lambda _: faker.simple_profile()['name'])
    dni = factory.LazyAttribute(lambda _: faker.bothify(text='##########'))
    birthday = factory.LazyAttribute(lambda _: faker.simple_profile()['birthdate'])
    position = factory.LazyAttribute(
        lambda _: faker.random_element(elements=('supervisor', 'logistica', 'ventas', 'obrero'))
    )
    profession = factory.LazyAttribute(
        lambda _: faker.random_element(elements=('Ingeniero', 'Abogado', 'Medico'))
    )
    has_children = factory.LazyAttribute(lambda _: faker.pybool())


class EmploymentRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = EmploymentRecord

    employee = factory.SubFactory(EmployeeFactory)
    company_name = factory.LazyAttribute(lambda _: faker.company())
    duration = factory.LazyAttribute(lambda _: faker.random_number(1))
    position = factory.LazyAttribute(
        lambda _: faker.random_element(elements=('supervisor', 'logistica', 'ventas', 'obrero'))
    )
