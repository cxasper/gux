# Django imports
from django.urls import reverse
from django.forms.models import model_to_dict

# Third Parties
from rest_framework import status
from rest_framework.test import APITestCase

# Local
from employee.models import Employee, EmploymentRecord
from apps.employee.tests.factories import EmployeeFactory, EmploymentRecordFactory


class EmployeeAPITests(APITestCase):
    def setUp(self):
        self.employee = EmployeeFactory()
        self.list_url = reverse('employee-list')
        self.detail_url = reverse(
            'employee-detail',
            kwargs={'pk': self.employee.pk}
        )

    def test_list(self):
        self.assertEqual(len(Employee.objects.all()), 1)
        Employee.objects.bulk_create(EmployeeFactory.build_batch(10))

        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 11)

    def test_list_with_filters(self):
        Employee.objects.bulk_create(EmployeeFactory.build_batch(10))

        # List with filter by birthday
        response = self.client.get(self.list_url, {'birthday':  self.employee.birthday})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

         # List with filter by dni
        response = self.client.get(self.list_url, {'dni':  self.employee.dni})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_retrieve(self):
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        self.assertEqual(len(Employee.objects.all()), 1)

        data = model_to_dict(EmployeeFactory.build())
        response = self.client.post(self.list_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(Employee.objects.all()), 2)

    def test_bad_create(self):
        self.assertEqual(len(Employee.objects.all()), 1)

        data = model_to_dict(EmployeeFactory.build(name=None))
        response = self.client.post(self.list_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete(self):
        self.assertEqual(len(Employee.objects.all()), 1)

        response = self.client.delete(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(Employee.objects.all()), 0)
    
    def test_patch(self):
        data = model_to_dict(EmployeeFactory.build())

        response = self.client.patch(self.detail_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], data['name'])

    def test_bad_patch(self):
        data = {'dni': 'invalid dni'}

        response = self.client.patch(self.detail_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class EmploymentRecordAPITests(APITestCase):
    def setUp(self):
        self.employee = EmployeeFactory()
        self.employment_record = EmploymentRecordFactory()
        self.list_url = reverse('employmentrecord-list')
        self.detail_url = reverse(
            'employmentrecord-detail',
            kwargs={'pk': self.employment_record.pk}
        )

    def test_list(self):
        self.assertEqual(len(EmploymentRecord.objects.all()), 1)
        EmploymentRecord.objects.bulk_create(
            EmploymentRecordFactory.build_batch(10, employee=self.employee)
        )

        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 11)
    
    def test_list_with_filters(self):
        other_employee = EmployeeFactory()
        EmploymentRecord.objects.bulk_create(
            EmploymentRecordFactory.build_batch(5, employee=other_employee)
        )

        # List with filter by employee
        response = self.client.get(self.list_url, {'employee': other_employee.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_retrieve(self):
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        self.assertEqual(len(EmploymentRecord.objects.all()), 1)

        data = model_to_dict(EmploymentRecordFactory.build(employee=self.employee))
        response = self.client.post(self.list_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(EmploymentRecord.objects.all()), 2)

    def test_bad_create(self):
        self.assertEqual(len(EmploymentRecord.objects.all()), 1)

        data = model_to_dict(EmploymentRecordFactory.build(employee=None))
        response = self.client.post(self.list_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete(self):
        self.assertEqual(len(EmploymentRecord.objects.all()), 1)

        response = self.client.delete(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(EmploymentRecord.objects.all()), 0)
    
    def test_patch(self):
        data = model_to_dict(EmploymentRecordFactory.build(employee=self.employee))

        response = self.client.patch(self.detail_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['company_name'], data['company_name'])

    def test_bad_patch(self):
        data = {'position': 'invalid too long value in position'}

        response = self.client.patch(self.detail_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

