
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete


from employee.models import Employee, EmploymentRecord
from employee.replicate import handle_delete, handle_insert, handle_update


@receiver(post_save, sender=Employee)
def post_create_employee(sender, instance, created, **kwargs):
    if created:
        handle_insert(instance)
    else:
        handle_update(instance)


@receiver(post_delete, sender=Employee)
def post_delete_employee(sender, instance, **kwargs):
    handle_delete(instance)


@receiver(post_save, sender=EmploymentRecord)
def post_create_employment_record(sender, instance, created, **kwargs):
    if created:
        handle_insert(instance)
    else:
        handle_update(instance)


@receiver(post_delete, sender=EmploymentRecord)
def post_delete_employment_record(sender, instance, **kwargs):
    handle_delete(instance)
