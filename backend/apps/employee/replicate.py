# Django
from django.db import connections

# Third party apps imports

# Local

def handle_insert(instance):
    replica_connection = connections['db_replica']

    with replica_connection.cursor() as cursor:
        data = instance.__dict__
        del data['_state']

        column_names = ', '.join(list(data.keys()))
        column_values = ', '.join([f"'{value}'" if not isinstance(value, (int, bool)) else str(value) for value in data.values()])
        print(f"INSERT INTO {instance._meta.db_table} ({column_names}) VALUES ({column_values})")
        cursor.execute(f"INSERT INTO {instance._meta.db_table} ({column_names}) VALUES ({column_values})")

def handle_update(instance):
    replica_connection = connections['db_replica']
    with replica_connection.cursor() as cursor:
        data = instance.__dict__
        del data['_state']

        cursor.execute(f"UPDATE {instance._meta.db_table} SET {', '.join([f'{name} = %s' for name in data.keys()])} WHERE id = %s", [*data.values(), instance.id])

def handle_delete(instance):
    replica_connection = connections['db_replica']
    with replica_connection.cursor() as cursor:
        cursor.execute(f"DELETE FROM {instance._meta.db_table} WHERE id = {instance.pk}")
