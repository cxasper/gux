# Third Parties
from rest_framework import routers

# Local
from employee.viewsets import EmployeeViewSet, EmploymentRecordViewSet


router = routers.SimpleRouter()
router.register(r'employees', EmployeeViewSet)
router.register(r'employment-record', EmploymentRecordViewSet)
