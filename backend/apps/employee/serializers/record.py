# Third Parties
from rest_framework import serializers

# Local
from employee.models import EmploymentRecord


class EmploymentRecordSerializer(serializers.ModelSerializer):
    """
    Model serializer for employeee.
    """

    class Meta:
        model = EmploymentRecord
        fields = '__all__'