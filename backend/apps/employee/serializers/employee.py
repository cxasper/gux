# Third Parties
from rest_framework import serializers

# Local
from employee.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    """
    Model serializer for employeee.
    """

    class Meta:
        model = Employee
        fields = '__all__'