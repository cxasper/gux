# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import RegexValidator


POSITION = [
    ('supervisor', 'Supervisor'),
    ('logistica', 'Logistica'),
    ('ventas', 'Ventas'),
    ('obrero', 'Obrero')
]


class Employee(models.Model):
    name = models.CharField('Nombre', max_length=225)
    dni = models.CharField( 'Número de identidad',
        validators=[
            RegexValidator(
                regex=r'^\d{10}$',
                message='El DNI debe tener exactamente 10 dígitos.',
            ),
        ],
        max_length=10,
        unique=True,
    )
    birthday = models.DateField('Cumpleaños')
    position = models.CharField(
        'Cargo', max_length=25, choices=POSITION, default='obrero'
    )
    profession = models.CharField('Profesión', max_length=30, null=True)
    has_children = models.BooleanField('Hijos', default=False)
    nationality = models.CharField('Nacionalidad', max_length=30, null=True)

    def __str__(self):
        return f'{self.name} - {self.dni}'

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'


class EmploymentRecord(models.Model):
    employee = models.ForeignKey(Employee, related_name='record', on_delete=models.CASCADE)
    company_name = models.CharField('Nombre de la empresa', max_length=225)
    duration = models.PositiveIntegerField()
    position = models.CharField('Cargo', max_length=30)

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name = 'Employment Record'
        verbose_name_plural = 'Employments Record'